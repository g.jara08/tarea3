package com.example.tarea3.utils;

/**
 * Validaciones
 */
public class Validaciones {
  
  public boolean validar_nombre(String nombre){
    if(nombre.length()>14 || nombre.length()<3){
      return false;
    }
    if(nombre.matches(".*[0-9\\p{Punct}].*")){
      return false;
    }
    return true;
  }

  public boolean validar_rut(String rut){
    boolean validacion = false;
    
    try{
      rut = rut.toUpperCase();
      rut = rut.replace(".", "");
      rut = rut.replace("-", "");
      int rutAux = Integer.parseInt(rut.substring(0, rut.length()-1));

      char dv = rut.charAt(rut.length()-1);

      int m = 0, s = 1;
      for(; rutAux != 0; rutAux /= 10){
        s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
      }
      if(dv == (char) (s != 0 ? s + 47 : 75)){
        validacion = true;
      }
    }catch (Exception ignored){

    }
    return validacion;
  }

  public boolean validar_numero_telefono(int numero_telefono){
    String nro_tel = String.valueOf(numero_telefono);
    if(nro_tel.length()!=9){
      return false;
    }
    if(numero_telefono<0){
      return false;
    }
    return true;
  }

  public boolean validar_edad(int edad){
    if(edad<0){
      return false;
    }
    return true;
  }

}
