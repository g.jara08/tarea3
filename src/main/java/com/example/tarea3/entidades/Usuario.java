package com.example.tarea3.entidades;

import lombok.Getter;
import lombok.Setter;

/**
 * Usuario
 */
@Getter @Setter
public class Usuario {

  private String nombre;
  private String apellido_paterno;
  private String apellido_materno;
  private String rut;
  private int numero_telefono;
  private int edad;

  public Usuario(){}

  public Usuario(
      String nombre,
      String apellido_paterno,
      String apellido_materno,
      String rut,
      int numero_telefono,
      int edad
      ){
    this.nombre = nombre;
    this.apellido_paterno = apellido_paterno;
    this.apellido_materno = apellido_materno;
    this.rut = rut;
    this.numero_telefono = numero_telefono;
    this.edad = edad;
      }

}
