package com.example.tarea3.controladores;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.tarea3.entidades.Usuario;
import com.example.tarea3.utils.Validaciones;

/**
 * MainController
 */
@RestController
@RequestMapping("/api")
public class MainController {

  Validaciones validador = new Validaciones();

  @PostMapping("/crear")
  public String crear(@RequestBody Usuario usuario_nuevo){

    Usuario usuario = usuario_nuevo;

    if(!validador.validar_nombre(usuario.getNombre())){
      return ("Nombre/Apellido demasiado largo o contiene números.\nNombre/Apellido debe tener 14 caractéres y no contener números.");
    }
    if(!validador.validar_rut(usuario.getRut())){
      return ("R.U.T. No es válido.");
    }
    if(!validador.validar_numero_telefono(usuario.getNumero_telefono())){
      return ("Número de telefono debe tener 9 dígitos y no ser negativo.");
    }
    if(!validador.validar_edad(usuario.getEdad())){
      return ("Edad debe ser un entero no negativo.");
    }
    return ("Usuario creado correctamente.");

  }
  
}
