package com.example.tarea3.utils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.*;

/**
 * ValidacionesTests
 */
public class ValidacionesTests {

  Validaciones validador;

  @BeforeEach
  void setUp(){
    validador = new Validaciones();
  }

  @Test
  @DisplayName("Test Nombre/Apellidos con más de 14 caracteres")
  public void test_largo_nombre(){
    String nombre = "xXDestroyerOfCatsXx";

    assertFalse(validador.validar_nombre(nombre));
  }

  @Test
  @DisplayName("Test Nombre/Apellidos contienen números")
  public void test_nombre_con_numeros(){
    String nombre = "Destroyer69420";

    assertFalse(validador.validar_nombre(nombre));
  }

  @Test
  @DisplayName("Test R.U.T.")
  public void test_rut(){
    String rut = "19.761.811-6";

    assertTrue(validador.validar_rut(rut));
  }

  @Test
  @DisplayName("Test Numero de Telefono distinto de 9 dígitos")
  public void test_numero_telefono(){
    int numero1 = 1234567891;
    int numero2 = 12345678;

    assertFalse(validador.validar_numero_telefono(numero1));
    assertFalse(validador.validar_numero_telefono(numero2));
  }

  @Test
  @DisplayName("Test Numero de Telefono no positivo")
  public void test_numero_no_valido(){
    int numero = -123456789;

    assertFalse(validador.validar_numero_telefono(numero));
  }

  @Test
  @DisplayName("Test Edad no positiva")
  public void test_edad_no_positiva(){
    int edad = -10;

    assertFalse(validador.validar_edad(edad));
  }
  
}
